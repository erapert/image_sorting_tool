require 'fileutils'
# Ubuntu 16.04: `sudo apt-get install libtcltk-ruby`
require 'tk'
# themes and new-style widgets
require 'tkextlib/tile'
# support for civilized and common image formats (.jpg, .png, etc)
# Ubuntu 16.04: `sudo apt-get install libtk-img`
require 'tkextlib/tkimg'

class HotkeysWindow
	attr_accessor :hotkeys

	def initialize parent
		@parent = parent
		
		path_entries = []

		@hotkey_window = TkToplevel.new @parent.root_win
		10.times.each do |index|
			Tk::Tile::Label.new(@hotkey_window) { text "When I press #{index.to_s} move the image to:" }.grid( :row => index, :column => 0, :sticky => 'w' )
			path_entry = Tk::Tile::Entry.new(@hotkey_window).grid( :row => index, :column => 1, :sticky => 'w' )
			path_entries.push path_entry
			Tk::Tile::Button.new(@hotkey_window) {
				text 'Set path'
				command(proc{
					path_entry.value = Tk::chooseDirectory
				})
			}.grid( :row => index, :column => 2, :sticky => 'w' )
		end

		# defining these here so we can access instance vars in such a way that they survive being passed through several blocks to the button commands
		onok = proc {
			@parent.set_hotkeys(path_entries.map { |e| e.value if e.value and e.value != '' })
			@hotkey_window.destroy
		}
		oncancel = proc {
			@hotkey_window.destroy
		}
		Tk::Tile::Button.new(@hotkey_window) { text 'Ok'; command onok }.grid( :row => 11, :column => 0 )
		Tk::Tile::Button.new(@hotkey_window) { text 'Cancel'; command oncancel }.grid( :row => 11, :column => 1 )
	end
end

class ImageSorter
	attr_accessor :root_win, :hotkeys

	def initialize
		@hotkeys = {}
		@image_list = []
		@current_img_index = 0
		
		build_gui
	end

	def build_gui
		@root_win = TkRoot.new { title 'Image Sorter' }
		
		menubar = TkMenu.new @root
			mfile = TkMenu.new menubar
				mfile.add :command, :label => 'Set Hotkeys', :command => proc{ show_hotkeys_window }, :accelerator => 'ctrl + h'
				mfile.add :command, :label => 'Open Directory', :command => proc{ open_dir }, :accelerator => 'ctrl + o'
				mfile.add :command, :label => 'Quit', :command => proc{ exit }, :accelerator => 'ctrl + q'
			menubar.add :cascade, :menu => mfile, :label => 'File'
		@root_win['menu'] = menubar
		
		#@image_frame = TkFrame.new(@root_win){ background '#000000' }.pack( :fill => 'both', :expand => 'yes' )
		@image_display = Tk::Tile::Label.new(@root_win){ background '#000000' }.pack( :fill => 'both', :expand => 'yes' )
		
		@root_win.bind 'Control-h', proc{ show_hotkeys_window }
		@root_win.bind 'Control-o', proc{ open_dir }
		@root_win.bind 'Control-q', proc{ exit }
		
		@root_win.bind 'Left', proc{ on_prev }
		@root_win.bind 'Right', proc{ on_next }
		@root_win.bind 'Key', proc{ |k| on_hotkey k }, "%A"

		return @root_win
	end

	def open_dir
		@top_level_dir = Tk::chooseDirectory
		return unless Dir.exists? @top_level_dir
		#puts "open '#{@top_level_dir}'"
		@image_list = find_all_images @top_level_dir
		@image_list.sort!
		@current_img_index = 0
		
		set_image @image_list[@current_img_index]
	end

	def show_hotkeys_window
		hkw = HotkeysWindow.new self
	end

	def set_hotkeys keys
		#keys.each do |k, hk|
		#	puts "set hotkey #{hk} (48 + #{hk.key} = #{hk.ascii_key}) => #{hk.path}"
		#end
		@hotkeys = keys
	end
	
	def on_prev
		@current_img_index = @current_img_index - 1
		@current_img_index = @image_list.length - 1 if @current_img_index < 0
		set_image @image_list[@current_img_index]
	end
	
	def on_next
		@current_img_index = @current_img_index + 1
		@current_img_index = 0 if @current_img_index > @image_list.length - 1
		set_image @image_list[@current_img_index]
	end
	
	def on_hotkey k
		return if @hotkeys[k.to_i].nil? or @hotkeys[k.to_i] == ''
		
		source = @image_list[@current_img_index]
		return if source.nil? or source == '' or !File.exists? source

		dest = File.join @hotkeys[k.to_i], File.basename(source)
		return if dest.nil? or dest == '' or !File.exists? File.dirname(dest)
		return if File.dirname(source) == File.dirname(dest)

		puts "move #{source} => #{dest}"
		
		FileUtils.mv source, dest
		@image_list.delete_at @current_img_index
		
		set_image @image_list[@current_img_index]
	end
	
	# return an array of all image paths under path (recursive)
	def find_all_images path
		# looks in path and also in sub-dirs of path
		return Dir[
			"#{path}/**/*.jpg",
			"#{path}/**/*.png",
			"#{path}/**/*.gif"
		]
	end
	
	# show the image at path if it exists
	def set_image path
		if !path.nil? and File.exists? path
			#puts "Go to '#{path}'"
			tmp = TkPhotoImage.new( :file => path )
			img = TkPhotoImage.new()
			
			# we must manually figure out how much to subsample it to make it fit
			# because Tk sucks at handling images so we can't just say "shrink or zoom to fit"
			image_ratio = tmp.width.to_f / tmp.height.to_f
			window_ratio = @root_win.winfo_width.to_f / @root_win.winfo_height.to_f
			
			if window_ratio > image_ratio
				fit_res = [(tmp.width.to_f * @root_win.winfo_height.to_f / tmp.height.to_f).to_i, @root_win.winfo_height]
			else
				fit_res = [@root_win.winfo_width, (tmp.height.to_f * @root_win.winfo_width.to_f / tmp.width.to_f).to_i]
			end
			
			subsample = [(tmp.width.to_f / fit_res[0].to_f).ceil, (tmp.height.to_f / fit_res[1].to_f).ceil]
			subsample[0] = 1 if subsample[0] < 1
			subsample[1] = 1 if subsample[1] < 1
			
			#puts "#{image_ratio}, #{window_ratio} -> #{fit_res}, #{subsample} ; #{tmp.width}, #{tmp.height} ; #{@root_win.winfo_width}, #{@root_win.winfo_height}"
			
			img.copy( tmp, :subsample => subsample )
			tmp = nil
			@image_display['image'] = img
			
			@root_win['title'] = path
		else
			raise "Image path not found: '#{path}' (has the image been moved or deleted?)"
		end
	end
end


if __FILE__ == $0
	i = ImageSorter.new

	Tk.mainloop
end
