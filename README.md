Image Sorting Tool
==================

Ruby + Tk tool to sort image files.

Say you've got a directory with lots of image files in it and you want a tool that
will let you flick through them and with the push of a single button will send the
current image to a predefined directory... that's what this tool does.

Requirements
------------

* ruby 2.0.0+
* tk
* tk image extension

Ubuntu 16.04:

`sudo apt-get install ruby libtcltk-ruby libtk-img`

Usage
-----

`ruby ./image_sorter.rb`

Select a directory to load all images from it -- `ctrl + o`.

Press `left` and `right` arrow keys to go to the previous or next.

Press `0-9` to send the image to a directory set to the hotkey (`ctrl + h` to set hotkeys).
